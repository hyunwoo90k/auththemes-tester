# AuthThemes Tester

## Prerequisites

- Node (I recommend using v12 or higher)
- golang 1.13

For OSX users these commands should get you there:

    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
    nvm install lts/erbium
    nvm alias default lts/erbium
    brew install go --with-cc-all

## Development Environment

Please set path to local directory where you cloned auththemes repository to "AUTHTHEMES_PATH" env

    AUTHTHEMES_PATH="../auththemes" go run main.go

open http://127.0.0.1:9000/auth/login
You can directly edit template files inside the "auththemes" directory, refresh browser and see changes.
