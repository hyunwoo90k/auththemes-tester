module bitbucket.org/hyunwoo90k/auththemes-tester

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/gosimple/slug v1.9.0 // indirect
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/microcosm-cc/bluemonday v1.0.2 // indirect
	github.com/qor/admin v0.0.0-20200315024928-877b98a68a6f // indirect
	github.com/qor/assetfs v0.0.0-20170713023933-ff57fdc13a14 // indirect
	github.com/qor/auth v0.0.0-20190603071357-709ba0ce943b
	github.com/qor/cache v0.0.0-20171031031927-c9d48d1f13ba // indirect
	github.com/qor/i18n v0.0.0-20181014061908-f7206d223bcd
	github.com/qor/mailer v0.0.0-20180329083248-0555e49f99ac // indirect
	github.com/qor/middlewares v0.0.0-20170822143614-781378b69454 // indirect
	github.com/qor/qor v0.0.0-20200224122013-457d2e3f50e1
	github.com/qor/redirect_back v0.0.0-20170907030740-b4161ed6f848 // indirect
	github.com/qor/render v1.1.1
	github.com/qor/responder v0.0.0-20171031032654-b6def473574f // indirect
	github.com/qor/roles v0.0.0-20171127035124-d6375609fe3e // indirect
	github.com/qor/session v0.0.0-20170907035918-8206b0adab70
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46 // indirect
	github.com/theplant/cldr v0.0.0-20190423050709-9f76f7ce4ee8 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
